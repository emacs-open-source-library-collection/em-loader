#!/bin/sh
# Example of setting up your own custom emacs init using em-loader
mkdir -p $HOME/myem/bundles/
cp ./init.el $HOME/myem/
cd $HOME/myem
touch myem
chmod +x myem
echo '#!/bin/bash
EMACS_USER_DIRECTORY=$HOME/myem emacs -q --load "$HOME/myem/init.el"' > myem
