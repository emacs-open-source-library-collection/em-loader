#+TITLE: EM Loader - Emacs org settings helper

Follow the below code to get a basic setup working if your new to emacs.


* Example setup
This example creates a new config for emacs and renames everything so you launch myem you could change this to what ever you like.
If you want to use the standard .emacs.d and emacs command just use the init examples.

It is worth creating a new folder in HOME like myem and placing a launcher script like below.
This will allow you to have your setup seperate to the standard emacs config.

** Optional example setup myem folder
Run with =sh setup-example.sh= from the checked out repository, or create a folder and run these commands from with in.
#+BEGIN_SRC shell :tangle setup-example.sh
mkdir -p $HOME/myem/bundles/
cd $HOME/myem
touch myem
chmod +x myem
echo '
#!/bin/bash
EMACS_USER_DIRECTORY=$HOME/myem emacs -q --load "$HOME/myem/init.el"
' > myem
#+END_SRC


** Basic init.el - EM loader from use-package
#+BEGIN_SRC emacs-lisp
;;Important because we start emacs with --load init.el we need to update the paths
;;So everything is relative to the launch folder.
(setq inhibit-startup-message t)
(setq user-init-file (or load-file-name (buffer-file-name)))
(setq user-emacs-directory (file-name-directory user-init-file))

;;required files to bootstrap our environment.
(require 'org)
(require 'package)

;;setup your package repos
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives
             '("melpa2" . "http://www.mirrorservice.org/sites/melpa.org/packages/"))
(add-to-list 'package-archives
             '("melpa3" . "http://www.mirrorservice.org/sites/stable.melpa.org/packages/"))
(add-to-list 'package-archives
             '("org" . "https://orgmode.org/elpa/") t)
(package-initialize)

;; Bootstrap `use-package'
(package-initialize)
(unless (package-installed-p 'use-package)
        (package-refresh-contents)
        (package-install 'use-package))

;;Use em loader
(use-package em-loader :ensure em-loader)
(em-loader-load-configs)
#+END_SRC


** Basic init.el - EM loader direct from git
Use quelpa and pull from gitlab direclty 
#+BEGIN_SRC emacs-lisp :tangle init.el
;;Important because we start emacs with --load init.el we need to update the paths
;;So everything is relative to the launch folder.
(setq inhibit-startup-message t)
(setq user-init-file (or load-file-name (buffer-file-name)))
(setq user-emacs-directory (file-name-directory user-init-file))

;;required files to bootstrap our environment.
(require 'org)
(require 'package)

;;Enable `use-package` and add package repositories
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))

;; Bootstrap `use-package'
(package-initialize)
(unless (package-installed-p 'use-package)
        (package-refresh-contents)
        (package-install 'use-package))


;; Add Quelpa to install direct from git
(use-package quelpa :ensure quelpa)
(use-package quelpa-use-package :ensure quelpa-use-package)

(quelpa '(em-loader :fetcher gitlab :repo "emacs-open-source-library-collection/em-loader" :branch "master"))
(em-loader-load-configs)
#+END_SRC
